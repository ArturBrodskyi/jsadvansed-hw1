class Employee {
    constructor(stats) {
        this._age = stats.age
        this._name = stats.name
        this._salary = stats.salary
    }

    get age() {
        return this._age;
    }

    get name() {
        return this._name;
    }

    get salary() {
        return this._salary;
    }

    set name(newName) {
        this._name = newName;
    }

    get salary() {
        return this._salary;
    }

    set salary(newSalary) {
        this._salary = newSalary;
    }

}


class Programmer extends Employee {
    constructor(stats) {
        super(stats);
        this._lang = stats.lang
    }

    get salary() {
        return this._salary * 3;
    }
}

const Artur = new Programmer({
    name: 'Artur',
    age: 1500,
    salary: 45000,
    lang: 'english, ukrainian'
})

const Ihor = new Programmer({
    name: 'Ihor',
    age: 58,
    salary: 80000,
    lang: 'english, ukrainian, spanish'
})

const John = new Programmer({
    name: 'John',
    age: 43,
    salary: 10000,
    lang: 'english, spanish'
})

console.log(John);
console.log(Ihor);
console.log(Artur);


// 1 Прототипне наслідування ніби як шаблон, який можно брати за основу класу і змінювати деякі аспекти в ньому.
// 2 Щоб клас спадкоємець мав аргументи, які викликаються у його батьківського класу